const express = require('express');
var path = require('path');
const app = express();
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, 'public')));
app.get('/', (req, res) => {
    res.render('home');
});
app.get('/about', (req, res) => {
    res.render('about');
});
app.get('/companies', (req, res) => {
    res.render('companies');
});
app.get('/students', (req, res) => {
    res.render('students');
});
app.get('/mentors', (req, res) => {
    res.render('mentors');
});
app.get('/mentor', (req, res) => {
    res.render('mentor');
});
app.get('/trainings', (req, res) => {
    res.render('trainings');
});
app.get('/training', (req, res) => {
    res.render('training');
});
app.get('/contact', (req, res) => {
    res.render('contact');
});
app.listen('3000', () => {
    console.log('Express is working.');
});